############################
# STEP 0 get dependencies
############################
FROM golang:1.17.6-alpine3.15 AS dependencies
WORKDIR /go/src
COPY go.mod .
COPY go.sum .
RUN go mod download
############################
# STEP 1 build executable binary
############################
FROM dependencies AS builder
LABEL maintainer="art.frela@gmail.com" version="0.0.1"
ARG BUILD_NUMBER
ARG GIT_HASH
ENV BUILD_NUMBER ${BUILD_NUMBER}
ENV GIT_HASH ${GIT_HASH}
##
ENV GO111MODULE=on \
  CGO_ENABLED=0 \
  GOOS=linux \
  GOARCH=amd64

WORKDIR /go/src
COPY . .
##
RUN go build -o /bin/simplestapi .
############################
# STEP 2 build a small image
############################
FROM alpine:3.15.0
RUN apk add --no-cache tzdata wget
ENV TZ=Europe/Moscow

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
COPY --from=builder /bin/simplestapi /bin/simplestapi
WORKDIR /bin
CMD ["./simplestapi", "-p=8080"]